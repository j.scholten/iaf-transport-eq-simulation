# IaF-transport eq simulation

This code is used, to simulate and integrate-and-fire type transport equation and was developed during my master thesis.

## Simulations

With the code, one can generate simulation such as the following.

Oscilations large c1 and small c2:
![dada](./gifs/ab.gif)

Instability of everything for large c1 and c2:
![dada](./gifs/cc.gif)
![dada](./gifs/cf.gif)

Stability for small c1
![dada](./gifs/f.gif)

Instability of high firing rate:
![dada](./gifs/e.gif)

## Datafiles

I do not include the real data files, as they are huge.

