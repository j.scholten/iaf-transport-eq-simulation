import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt
# Phi(v) = Phi*1_(v\ge v_1) and V_0(N) = e^{c1 -c2 min(N,L)}/(e^{...}-1)v_1 with L large, such that the resulting N is less than L. An then we take V_E huge.
dateiname = input("Dateiname: ")
f = open(dateiname,"r")
print(f.readline())

plt.ion()
fig, (ax1, ax2) = plt.subplots(2)
ax2.set_title("N(t)")
ax1.set_title("n")

Ns = []
ts = []
Vspace = [0]
n = [0]
linen, = ax1.plot(Vspace,n,label=f"n(t)")
lineN, = ax2.plot(ts,Ns,label=f"N(t)")
for line in f.readlines():
    ts.append(float(line.split(" ")[0]))
    Ns.append(float(line.split(" ")[1]))
    Vspace = [float(e) for e in line.split(" ")[2].split(",")]
    n = [float(e) for e in line.split(" ")[3].split(",")]
    lineN.set_xdata(ts)
    lineN.set_ydata(Ns)
    ax2.relim()
    ax2.autoscale_view(True,True,True)
    linen.set_xdata(Vspace)
    linen.set_ydata(n)
    ax1.relim()
    ax1.autoscale_view(True,True,True)
    fig.canvas.draw()
    fig.canvas.flush_events()
