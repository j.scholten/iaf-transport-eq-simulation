import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as integrate
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
    "savefig.bbox": "tight"
})

daten = [
        ["b",1/3],
        ["c",1/3],
        ["e",1/3],
        ["f",1/3],
        ["ab",1/2],
        ["ac",1/2],
        ["ae",1/3],
        ["af",1/2],
        ["bb",1/3],
        ["bc",1/2],
        ["be",1/3],
        ["bf",1/3],
        ["cb",1/3],
        ["cc",1/3],
        ["ce",1/3],
        ["cf",1/3],
        ]

def makeplots(data):
    dateiname = data[0]
    f = open(dateiname+".txt","r")
    print(f.readline())
    Ns = []
    ts = []
    for line in f.readlines():
        ts.append(float(line.split(" ")[0]))
        Ns.append(float(line.split(" ")[1]))
    factor = data[1]
    matplotlib.rcParams.update({"figure.figsize":(6.81236*factor,6.81236/3)})
    plt.xlabel("t")
    plt.plot(ts,Ns,label="N(t)")
    plt.legend()
    plt.savefig("./thick/"+dateiname+"N.pgf")
    plt.close()
    f.close()
for data in daten:
   makeplots(data)
