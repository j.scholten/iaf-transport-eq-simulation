import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from pylab import *
dateinamen = ["ab","cc","cf","e","f"]
def makeGif(namen):
    f = open("film/"+name+".txt","r")
    print(f.readline())

    fig, (ax1, ax2) = plt.subplots(2)
    ax2.set_title("N(t)")
    ax1.set_title("n")
    plt.tight_layout()

    Ns = []
    ts = []
    Vspaces = []
    ns = []
    for line in f.readlines():
        ts.append(float(line.split(" ")[0]))
        Ns.append(float(line.split(" ")[1]))
        Vspaces.append([float(e) for e in line.split(" ")[2].split(",")])
        ns.append([float(e) for e in line.split(" ")[3].split(",")])
    f.close()
    linen, = ax1.plot(Vspaces[0],ns[0],label=f"n(t)")
    lineN, = ax2.plot(ts[:1],Ns[:1],label=f"N(t)")
    def update(i):
        lineN.set_xdata(ts[:i])
        lineN.set_ydata(Ns[:i])
        ax2.relim()
        ax2.autoscale_view(True,True,True)
        linen.set_xdata(Vspaces[i])
        linen.set_ydata(ns[i])
        ax1.relim()
        ax1.autoscale_view(True,True,True)
        #fig.canvas.draw()
        #fig.canvas.flush_events()
        return linen,lineN
    ani = animation.FuncAnimation(fig,update,frames = range(0,len(ts),2))
    writer = animation.writers['ffmpeg'](fps=30)
    ani.save("gifs/"+name+".gif",writer=writer,dpi=100)
    plt.close(fig)
for name in dateinamen:
    makeGif(name)
