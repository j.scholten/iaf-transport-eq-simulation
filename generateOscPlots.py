import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as integrate
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
    "savefig.bbox": "tight"
})

def makeN(dateiname,t1,t2,factor,timepoints):
    f = open(dateiname+".txt","r")
    print(f.readline())
    Ns = []
    ts = []
    Vspaces = []
    ns = []
    for line in f.readlines():
        t = float(line.split(" ")[0])
        if t>t1 and t<t2: 
            ts.append(float(line.split(" ")[0]))
            Ns.append(float(line.split(" ")[1]))
    matplotlib.rcParams.update({"figure.figsize":(6.81236*factor,6.81236/3)})
    plt.xlabel("t")
    plt.plot(ts,Ns,label="N(t)")
    for times in timepoints:
        for t in times:
            plt.axvline(t,color="black",linestyle = ":")
    plt.legend()
    plt.savefig("./thick/oscN.pgf")
    plt.close()
    f.close()


def makeDens(dateiname,tplot,factor,ort,colors):
    f = open(dateiname+".txt","r")
    print(f.readline())
    Ns = []
    ts = []
    Vspaces = []
    ns = []
    currentt = 0
    for line in f.readlines():
        t=float(line.split(" ")[0])
        if currentt<len(tplot) and t>= tplot[currentt]:
            Vspaces.append([float(e) for e in line.split(" ")[2].split(",")])
            ns.append([float(e) for e in line.split(" ")[3].split(",")])
            tplot[currentt]=t
            currentt+=1
        #print(integrate.trapezoid(ns[-1],Vspaces[-1]))
    matplotlib.rcParams.update({"figure.figsize":(6.81236*factor,6.81236/3)})
    plt.xlabel("v")
    for i in range(0,len(tplot)):
        string = f"n(v,{tplot[i]:.2f})"
        plt.plot(Vspaces[i],ns[i],label=string,c=colors[i])
    plt.legend()
    plt.savefig("./thick/osc"+ort+"Densities.pgf")
    plt.close()
    f.close()
timepoints = [[109.26,109.89,110.52],[111,111.6,111.9],[112,112.36,112.73]]
"""
makeN("bc",140,150,1,timepoints)
makeN("bb",100,130,1,timepoints)
makeN("bf",70,90,1,timepoints)
"""
makeN("ab",98,114,1,timepoints)
makeDens("ab",timepoints[0],1/3,"up",["blue","orange","green"])
makeDens("ab",timepoints[1],1/3,"down",["red","purple","brown"])
makeDens("ab",timepoints[2],1/3,"bottom",["pink","grey","blue"])
