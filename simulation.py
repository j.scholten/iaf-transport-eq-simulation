import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt
# Phi(v) = Phi*1_(v\ge v_1) and V_0(N) = e^{c1 -c2 min(N,L)}/(e^{...}-1)v_1 with L large, such that the resulting N is less than L. An then we take V_E huge.
absoleps = 1e-8 #error tollerane in the integrals. 
releps = 0

while True:
    decision = input("Wählen Sie einen Wert für c1 aus:\na=3.5\nb=2\nIhre Eingabe (a oder b): ")
    if decision=="a":
        c1=3.5
        break
    if decision=="b":
        c1=2
        break
phi = 2.
v1 = 0.4
while True:
    decision = input("Wählen Sie einen Wert für c2 aus:\na: 1.8\nb: 2.8\nc:3.8\nd:1.5\nIhre Eingabe(a, b, c oder d): ")
    if decision=="d":
        c2=1.5
        break
    if decision=="a":
        c2 = 1.8
        break
    elif decision=="b":
        c2=2.8
        break
    elif decision=="c":
        c2=3.8
        break
Nplus = (c1+1/phi+np.sqrt((c1+1/phi)**2-4*c2))/2/c2
Nminus = (c1+1/phi-np.sqrt((c1+1/phi)**2-4*c2))/2/c2
print(Nplus)
print(Nminus)
VE = c1/c2
L = (c1/c2+Nplus)/2


def Pi(v):
    if v<v1:
        return 0
    else:
        return phi
def e(c1,c2,N):
    return np.exp(c1-c2*N)

def V0(N):
    if N<=L:
        return (v1*e(c1,c2,N))/(e(c1,c2,N)-1)
    else:
        return (v1*e(c1,c2,L))/(e(c1,c2,L)-1)

Nspace = np.linspace(0,5,8000)
plt.plot(Nspace,[V0(N) for N in Nspace])
plt.show()

def nstat(v,nbar):
    if v<=v1:
        return nbar/(V0(nbar)-v)
    elif v1<v and v<V0(nbar):
        return nbar/np.power(V0(nbar)-v1,phi)*np.power(V0(nbar)-v,phi-1)
    else:
        return 0

def offset(v):
    if v<=0.22:
        return -1
    elif v<=0.44:
        return 1
    else:
        return 0

def initialMaker():
    def const(v):
        return 1/VE
    def box(v):
        if v<=0.5:
            return 2
        else:
            return 0
    def statlow(v):
        return nstat(v,Nminus)
    def statlowplus(v):
        return nstat(v,Nminus)+0.0001*offset(v)
    def statlowminus(v):
        return nstat(v,Nminus)-0.0001*offset(v)
    def stathighplus(v):
        return nstat(v,Nplus)+0.0001*offset(v)
    def stathighminus(v):
        return nstat(v,Nplus)-0.0001*offset(v)
    while True:
        decision = input("a: const\nb: box\nc: stationary lower fire\nd: stationary lower fire minus\ne: stationary higher fire plus\nf: stationary higher fire minus\nIhre Eingabe (a,b,c,d,e oder f) ")
        if decision=="a":
            return const, VE
        elif decision=="b":
            return box, 0.5
        elif decision=="c":
            return statlow,V0(Nminus)
        elif decision=="d":
            return statlowminus,V0(Nminus)
        elif decision=="e":
            return stathighplus,V0(Nplus)
        elif decision=="f":
            return stathighminus,V0(Nplus)

def calcNPhi(Vspace,n,discontinuity,previousi):
    j = 0
    for j in range(0,len(Vspace)):
        if Vspace[min(previousi+j,len(Vspace))]>v1 and Vspace[min(previousi+j-1,len(Vspace))]<=v1:
            i = previousi+j
            break
        if Vspace[max(0,previousi-j)]>v1 and Vspace[max(previousi-j-1,0)]<=v1:
            i = previousi-j
            break
    ghostvalue = (v1-Vspace[i-1])/(Vspace[i]-Vspace[i-1])*n[i]+(Vspace[i]-v1)/(Vspace[i]-Vspace[i-1])*n[i-1]
    if i<discontinuity:
        integrant1 = [ghostvalue]+n[i:discontinuity] 
        integrant2 = n[discontinuity:]
        int1 = integrate.trapezoid(integrant1,[v1]+Vspace[i:discontinuity])
        int2 = integrate.trapezoid(integrant2,Vspace[discontinuity:])
    else:
        int1 = 0
        integrant2 = [ghostvalue]+n[i:] 
        int2 = integrate.trapezoid(integrant2,[v1]+Vspace[i:])
    return (int1+int2)*phi, i
def calcN(Vspace,n,discontinuity):
    integrant1 = [n[i]*Pi(Vspace[i]) for i in range(0,discontinuity)]
    integrant2 = [n[i]*Pi(Vspace[i]) for i in range(discontinuity,len(Vspace))]
    int1 = integrate.trapezoid(integrant1,Vspace[:discontinuity])
    int2 = integrate.trapezoid(integrant2,Vspace[discontinuity:])
    return int1+int2
def hhelp(v,t,N):
    return V0(N)*(1-np.exp(-t))+np.exp(-t)*v
def h(Vspace,N,tStepSize):
    return [hhelp(v,tStepSize,N) for v in Vspace]
def scale(n,Vspace,N,tStepSize):
    newn =  [np.exp(tStepSize-integrate.quad(lambda s: Pi(hhelp(Vspace[i],s,N)),0,tStepSize,full_output=1)[0])*n[i] for i in range(0,len(n))]
    return newn
def abserror(N,Ns,tolerance):
    Nerror = abs(N-Ns[-1])<tolerance
    Verror = abs(V0(N)-V0(Ns[-1]))<tolerance
    BoundError = abs(N/V0(N)-Ns[-1]/V0(Ns[-1]))<tolerance
    return Nerror and Verror and BoundError
def relerror(N,Ns,tolerance):
    Nerror = abs(N-Ns[-1])/N<tolerance
    Verror = abs(V0(N)-V0(Ns[-1]))/V0(N)<tolerance
    return Verror and Nerror

def writefile(file,t,N,Vspace,n):
    file.write(str(t)+" "+str(N)+" "+",".join(str(e) for e in Vspace)+" "+",".join(str(e) for e in n)+"\n")

eps = 1e-4
mineps = 1e-6
maxTimeStepSize = 0.005
tolerance = 0.015
initialNumberOfPoints = 1500
n0,maxVal = initialMaker()
Vspace = [v for v in np.linspace(0,v1,initialNumberOfPoints,endpoint=False)] + [v for v in np.linspace(v1,maxVal,initialNumberOfPoints)]
Vspacetemp = Vspace
n = [n0(v) for v in Vspace]
ntemp = n
ts = [0]
N, currentv1 = calcNPhi(Vspace,n,0,initialNumberOfPoints)
Ns = [N]
newBound = N/V0(N)

plt.ion()
fig, (ax1, ax2) = plt.subplots(2)
ax2.set_title("N(t)")
ax1.set_title("n")
linen, = ax1.plot(Vspace,n,label=f"n(t)")
lineN, = ax2.plot(ts,Ns)
ax1.legend()

numberofIterations = 0
currentv1 = initialNumberOfPoints
currentv1temp = currentv1

DateiName = input("Dateiname: ")
savetimeStep = 0.005
lastsave = -savetimeStep
with open(DateiName,"w") as file:
    file.write("eps="+str(eps)+" mineps="+str(mineps)+" maxTimeStepSize="+str(maxTimeStepSize)+" tolerance="+str(tolerance)+" initialNumberOfPoints="+str(initialNumberOfPoints)+"\n")
    while True:
        n = [N/V0(N)]+ntemp
        Vspace = [0]+Vspacetemp
        if(ts[-1]-lastsave>=savetimeStep): 
            writefile(file,ts[-1],N,Vspace,n)
            lastsave = ts[-1]
        currentv1 = currentv1temp
        numberofIterations += 1
        eps =min(2*eps,maxTimeStepSize)
        while True: #find eps
            ntemp = scale(n,Vspace,Ns[-1],eps)
            Vspacetemp = h(Vspace,Ns[-1],eps)
            N,currentv1temp = calcNPhi([0]+Vspacetemp,[Ns[-1]/V0(Ns[-1])]+ntemp,numberofIterations+1,currentv1)
            if(abserror(N,Ns,tolerance) and relerror(N,Ns,tolerance)) or (eps*0.8<mineps):
                break
            else: 
                eps/=2
        ts.append(ts[-1]+eps)
        Ns.append(N)
        
        lineN.set_xdata(ts)
        lineN.set_ydata(Ns)
        ax2.relim()
        ax2.autoscale_view(True,True,True)
        linen.set_xdata(Vspace)
        linen.set_ydata(n)
        ax1.relim()
        ax1.autoscale_view(True,True,True)
        fig.canvas.draw()
        fig.canvas.flush_events()
    #plt.ioff()
    #plt.show()
